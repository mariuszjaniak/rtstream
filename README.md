# RtStream #

FreeRTOS interrupt driven stream implementation.  Main features 

* blocking read/write,
* based on binary semaphore, not counting semaphore,
* written in plain C, 
* integrated with FreeRTOS, 
* no hardware dependency

# Example #

Stream initialization

```
#!c
#define SERIAL_FIFO_RX_SIZE     160
#define SERIAL_FIFO_TX_SIZE     160

uint8_t   rx[SERIAL_FIFO_RX_SIZE];
uint8_t   tx[SERIAL_FIFO_TX_SIZE];
sRtStream stream;

rtStreamInit(&stream,
             rx,
             SERIAL_FIFO_RX_SIZE,
             rx,
             SERIAL_FIFO_TX_SIZE,
             uart_enTx);

```
Function *uart_enTx* starts UART transmission

```
#!c
void uart_enTx(void)
{
  /* Enable UART TX event */
  UART_EnableEvent(UART_DEV, UART_EVENT_TRANSMIT);
  );
  /*Trigger the transmit buffer interrupt*/
  TriggerServiceRequest(UART_DEV, UART_TX);
}

```
The receiver interrupt service routine 
```
#!c

void UART_Rx_IRQHandler(void)
{
  BaseType_t hptw = false;

  while(UART_IsEmpty(UART_DEV) == false){
    uint8_t data;
    /* Read the next character from the UART */
    data = UART_GetReceivedData(UART_DEV);
    /* Write it to FIFO */
    rtStreamIntRxByte(&stream, data);
  }
  /* Post process stream after reception */
  rtStreamIntRxBytePost(&stream, &hptw);
  portEND_SWITCHING_ISR(hptw);
}

```
The transmitter interrupt service routine 

```
#!c
void UART_Tx_IRQHandler(void)
{
  BaseType_t hptw = false;
  uint32_t   n;

  /* Get number of available data */
  if((n = rtStreamTxStatFromISR(&stream)) > 0){
    while((UART_IsFull(UART_DEV) == false) && n--){
      uint8_t data;
      /* Get byte */
      rtStreamIntTxByte(&stream, &data);
      /* Send data */
      UART_Transmit(UART_DEV, data);
    }
    /* Post process stream after transmition */
    rtStreamIntTxBytePost(&stream, &hptw);
  }
  else{
    /* Disable the standard transmit event */
    UART_DisableEvent(UART_DEV, UART_EVENT_TRANSMIT);
  }
  portEND_SWITCHING_ISR(hptw);
}

```

# Remarks #
This implementation requires modified version of the FreeRTOS, which implements semaphore give function from critical section. Suitable modified FreeRTOS can be found in Other folder.